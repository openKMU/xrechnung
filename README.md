# XRechnung

This is a collection of some utilities around the XRechnung Standard that is used in Germany and other countries for electronic invoicing.

This repository is young and the tools are in very early stage. It is a start to support this standard from open source. Contributions to this efforts are highly appreciated.

XRechnung documents can be created by the software [Kraft](https://volle-kraft-voraus.de) ([Github repo](https://github.com/dragotin/kraft)).

XRechnung samples can be found in the [xrechnung-testsuite repository](https://projekte.kosit.org/xrechnung/xrechnung-testsuite/-/tree/master/).

