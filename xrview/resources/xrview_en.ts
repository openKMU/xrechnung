<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.ui" line="14"/>
        <source>XRView</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="37"/>
        <source>Page 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="44"/>
        <source>about:blank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="63"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="71"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="84"/>
        <source>&amp;Open…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="87"/>
        <source>Open a XRechnung file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="90"/>
        <source>Ctrl+O</source>
        <comment>File open</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="98"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="101"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="104"/>
        <source>Exit the program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="107"/>
        <source>Ctrl+Q</source>
        <comment>Exit the program</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="115"/>
        <source>About XRView</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="112"/>
        <source>&amp;About XRView…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="43"/>
        <location filename="../src/mainwindow.cpp" line="91"/>
        <source>XRechnung Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="92"/>
        <source>The needed resources to display the document could not be found.

Please remove the configuration file ~/.config/xrview/xrview.conf and restart the viewerto foster the automatic download again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="117"/>
        <source>Resource Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="118"/>
        <source>Some of the required resource files could not be found.

Should they be downloaded from Github?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="120"/>
        <source>Start download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="128"/>
        <source>All resources loaded, ready to load ✔️</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="128"/>
        <source>Resource error, can not load XRechnung</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="137"/>
        <source>Resources not available, download not wanted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="142"/>
        <source>Not all resources available. See xrview README.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="145"/>
        <source>All resources available, ready to load XRechnung ✔️</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="210"/>
        <source>XRView - XRechnung Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="211"/>
        <source>&lt;b&gt;XRechnung Viewer Version %1&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;XRView is a utility to display XRechnung documents.&lt;br/&gt;&lt;br/&gt;This program is released under GPL v3.&lt;br/&gt;Please contribute at &lt;a href=&quot;https://codeberg.org/openKMU/xrechnung&quot;&gt;https://codeberg.org/openKMU/xrechnung&lt;/a&gt;&lt;br/&gt;&lt;br/&gt;Author: %2&lt;br/&gt;Copyright %3 Klaas Freitag</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/main.cpp" line="71"/>
        <source>xrechnung</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="71"/>
        <source>List of xrechnung</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="153"/>
        <source>Open XRechnung</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xrview.h" line="35"/>
        <source>Viewer for XRechnung documents.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xrview.h" line="36"/>
        <source>GNU General Public License Version 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xrview.h" line="37"/>
        <source>2023-2024</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>XRWidget</name>
    <message>
        <location filename="../src/xrwidget.cpp" line="59"/>
        <source>Leitweg-ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xrwidget.cpp" line="64"/>
        <source>Date:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xrwidget.cpp" line="66"/>
        <source>Buyer:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xrwidget.cpp" line="70"/>
        <source>Net:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xrwidget.cpp" line="76"/>
        <source>VAT:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xrwidget.cpp" line="82"/>
        <source>Gross:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>XRechnung</name>
    <message>
        <location filename="../src/xrechnung.cpp" line="141"/>
        <source>Error while creating UBL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xrechnung.cpp" line="179"/>
        <source>Error while creating HTML</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
