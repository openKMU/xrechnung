## XRView

XRView is a Qt based viewer for the Desktop that renders XRechnung XML documents. It uses the official XSL stylesheets provided in [the repository](https://github.com/itplr-kosit/xrechnung-visualization) of the [Koordinierungsstelle für IT-Standards](https://www.xoev.de/).

![XRView screenshot](/xrview/screenshot1.png?raw=true&s=200 "Screenshot")

Beside the rendering, it also extracts some important data from the XML and displays them in the overview pane.

### Configuration

XRView will download the required resources on first start if the user allows to do so.

That will download what is needed, unpack, and create a config file in `$HOME/.config/xrview/xrview.conf`. If you don't like that to happen you can disable this feature at build time.

The config file looks like:

```bash
[saxon]
jar=/real/path/saxon-he-11.4.jar
xslHtml=/real/path/xrechnung-visualization/src/xsl/xrechnung-html.xsl
xslUbl=/real/path/xrechnung-visualization/src/xsl/ubl-invoice-xr.xsl
xslUbl=/real/path/xrechnung-visualization/src/xsl/xr-pdf.xsl
```

The files are downloaded from this sources:

1. Saxon:  https://raw.githubusercontent.com/Saxonica/Saxon-HE/main/11/Java/SaxonHE11-4J.zip
2. xrechnung-visualization XSLT sheets: https://projekte.kosit.org/xrechnung/xrechnung-visualization/-/archive/master/xrechnung-visualization-master.zip

### Usage

Start the viewer from the command line and pass one or more XML files in XRechnung format on the command line.

Example:
```bash
xrview /real/path/3321-342.xml
```

There is also a file menu that allows opening XRechnung files in a file selector.

### Build

XRView is written in C++ using the Qt toolkit and cmake. Build as usual.

Hint: By as usual we mean, that you run the following command in the directory you found this readme in.
```sh
cmake .
make
```

Make sure you have the relevant development packages installed.

To build with the automatic download feature disabled:
```sh
cmake . -DWITH_AUTO_DOWNLOAD=OFF
make
```

When automatic downloading is disabled you don't need KF6Archive as a dependency.
