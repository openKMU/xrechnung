/*
 * XRechnung Viewer Suite
 *
 * Copyright (C) 2023 Klaas Freitag <opensource@freisturz.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "xrwidget.h"
#include "xrechnung.h"

#include <QFormLayout>
#include <QLabel>
#include <QLocale>
#include <QDate>
#include <QSizePolicy>

XRWidget::XRWidget(QWidget *parent)
    : QWidget{parent}
{

}

XRWidget::XRWidget(XRechnung *xr, QWidget *parent)
    : QWidget{parent},
      _xr{xr}

{
    _layout = new QFormLayout(this);

    connect(xr, &XRechnung::domDocAvailable, this, &XRWidget::slotDomAvailable);
}

QLabel* XRWidget::addDisplay(const QString& name, const QString& valueStr)
{
    QLabel *l = new QLabel(valueStr);
    l->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
    _layout->addRow(name, l);
    return l;
}

void XRWidget::slotDomAvailable()
{
    QString locVal;
    QLocale locale;

    locVal = _xr->value("xr:Buyer_reference");
    addDisplay(tr("Leitweg-ID:"), locVal);

    const QString d = _xr->value("xr:Invoice_issue_date"); // format 2024-04-17
    QDate date = QDate::fromString(d, Qt::ISODate);
    const QString dateFormat = locale.dateFormat(QLocale::ShortFormat);
    addDisplay(tr("Date:"), locale.toString(date, dateFormat));
    locVal = _xr->value("xr:Buyer_name");
    addDisplay(tr("Buyer:"), locVal);

    locVal = _xr->value("xr:Sum_of_Invoice_line_net_amount");
    double cur = locVal.toDouble();
    QLabel *l = addDisplay(tr("Net:"), locale.toCurrencyString(cur));
    l->setFrameStyle(QFrame::Panel | QFrame::Sunken);
    l->setAlignment(Qt::AlignRight|Qt::AlignCenter);

    locVal = _xr->value("xr:Invoice_total_VAT_amount");
    cur = locVal.toDouble();
    l = addDisplay(tr("VAT:"), locale.toCurrencyString(cur));
    l->setFrameStyle(QFrame::Panel | QFrame::Sunken);
    l->setAlignment(Qt::AlignRight|Qt::AlignCenter);

    locVal = _xr->value("xr:Invoice_total_amount_with_VAT");
    cur = locVal.toDouble();
    l = addDisplay(tr("Gross:"), locale.toCurrencyString(cur));
    l->setFrameStyle(QFrame::Panel | QFrame::Sunken);
    l->setAlignment(Qt::AlignRight|Qt::AlignCenter);
}


