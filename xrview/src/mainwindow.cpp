/*
 * XRechnung Viewer Suite
 *
 * Copyright (C) 2023 Klaas Freitag <opensource@freisturz.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "xrechnung.h"
#include "xrview.h"
#include "xrwidget.h"

#include <QFile>
#include <QFileInfo>
#include <QMessageBox>
#include <QTimer>
#include <QSettings>
#include <QObject>
#include <QRegularExpression>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->_mainToolBox->removeItem(0);

    connect(ui->_mainToolBox, &QToolBox::currentChanged, this, &MainWindow::slotShowDocumentNo);
    setWindowTitle(tr("XRechnung Viewer"));

    // restore window geometry
    QSettings config;
    const auto geo = QByteArray::fromBase64(config.value(GeoWindow).toByteArray());
    restoreGeometry(geo);

    const QStringList ints = config.value(GeoSplitter).toString().split('/');
    if (ints.size() > 1) {
        QList<int> s {ints.at(0).toInt(), ints.at(1).toInt()};
        ui->splitter->setSizes(s);
    }

    // The controller gets a list of urls to laod the XRechnung docs from.
    // This connection cares for displaying the XRechnung.
    QObject::connect(&_xrc, &XRControl::showXRechnung, this, &MainWindow::slotShowXRechnung);

    ui->actionSaveAs->setEnabled(false);
    ui->actionExportAsPdf->setEnabled(false);

    // connect menu items
    QObject::connect(ui->actionLoad, &QAction::triggered, this, &MainWindow::slotLoadFile);
    QObject::connect(ui->actionSaveAs, &QAction::triggered, this, &MainWindow::slotSaveAs);
    QObject::connect(ui->actionExportAsPdf, &QAction::triggered, this, &MainWindow::slotExportAsPdf);
    QObject::connect(ui->actionExit, &QAction::triggered, QCoreApplication::instance(), &QCoreApplication::quit);
    QObject::connect(ui->actionAbout_XRView, &QAction::triggered, this, &MainWindow::about);

    QTimer::singleShot(0, this, &MainWindow::checkConfig);
}

MainWindow::~MainWindow()
{
    QSettings config;
    const QByteArray geo = saveGeometry().toBase64();
    config.setValue(GeoWindow, QString::fromLatin1(geo));

    const QList<int> s = ui->splitter->sizes();
    if (s.size()>1)
        config.setValue(GeoSplitter, QString("%1/%2").arg(s.at(0)).arg(s.at(1)));

    config.sync();

    delete ui;
}

void MainWindow::checkConfig()
{
    auto stateMap = _setup.check();

#ifdef WITH_AUTO_DOWNLOAD
    if (stateMap[XRechnung::SaxonHtml] == XRVSetup::State::Error||
            stateMap[XRechnung::SaxonUbl] == XRVSetup::State::Error||
            stateMap[XRechnung::SaxonJar] == XRVSetup::State::Error) {
        QMessageBox::information(this, tr("XRechnung Viewer"),
                                 tr("The needed resources to display the document could not be found.\n\n"
                                    "Please remove the configuration file ~/.config/xrview/xrview.conf and restart the viewer"
                                    "to foster the automatic download again."));

        return;
    }
#endif

    if (stateMap[XRechnung::SaxonHtml] != XRVSetup::State::Available ||
            stateMap[XRechnung::SaxonUbl] != XRVSetup::State::Available ||
            stateMap[XRechnung::SaxonJar] != XRVSetup::State::Available) {

		qDebug() << "The following '[saxon]' resources are unvailable:";
		if (stateMap[XRechnung::SaxonJar] != XRVSetup::State::Available) {
			qDebug() << "  * The saxon-he jar file.";
		}
		if (stateMap[XRechnung::SaxonHtml] != XRVSetup::State::Available) {
			qDebug() << "  * The xslHtml file.";
		}
		if (stateMap[XRechnung::SaxonUbl] != XRVSetup::State::Available) {
			qDebug() << "  * The xslUbl file.";
		}
		qDebug() << "";
#ifdef WITH_AUTO_DOWNLOAD
		qDebug() << "Offering automatic download via GUI.";
        auto btn = QMessageBox::question(this, tr("Resource Download"),
                                        tr("Some of the required resource files could not be found.\n\nShould they be downloaded from Github?"));
        if (btn == QMessageBox::Yes) {
            statusBar()->showMessage(tr("Start download"));
            connect(&_setup, &XRVSetup::done, [=](QMap<QString, XRVSetup::State> states) {
                bool cool{true};
                if (states[XRechnung::SaxonHtml] != XRVSetup::State::Available ||
                    states[XRechnung::SaxonUbl] != XRVSetup::State::Available ||
                        states[XRechnung::SaxonJar] != XRVSetup::State::Available) {
                    cool = false;
                }
                statusBar()->showMessage(cool ? tr("All resources loaded, ready to load ✔️") : tr("Resource error, can not load XRechnung"));
            });

            // The controller gets a list of urls to laod the XRechnung docs from.
            // This connection cares for displaying the XRechnung.
            connect(&_setup, &XRVSetup::done, this, &MainWindow::slotShowFileList);
            _setup.doDownload(stateMap);
        } else {
            qDebug() << "No download wanted.";
            statusBar()->showMessage(tr("Resources not available, download not wanted."));
		}
#else
		qDebug() << "The automatic download feature has been disabled at compile time (i.e. by your distribution package).";
		qDebug() << "Please consult the xrview README to find out where to get the missing resources and how to configure them.";
		statusBar()->showMessage(tr("Not all resources available. See xrview README."));
#endif
    } else {
        statusBar()->showMessage(tr("All resources available, ready to load XRechnung ✔️"));
        QTimer::singleShot(0, this, &MainWindow::slotShowFileList);
    }

}

void MainWindow::slotLoadFile()
{
    QStringList filesStr = QFileDialog::getOpenFileNames(this, QObject::tr("Open XRechnung"),
                                                         QDir::homePath(),
                                                         "*.xml");
    QList<QUrl> urls;
    for( const QString& f : filesStr) {
        QUrl url{QUrl::fromLocalFile(f)};
        urls.append(url);
    }
    _xrc.registerFiles(urls);
    _files.append(urls);
}

void MainWindow::slotExportAsPdf()
{
    XRWidget *item = qobject_cast<XRWidget*>(ui->_mainToolBox->currentWidget());
    if (item != nullptr) {
        XRechnung *xr = item->xrechnung();
        QString pdfFilePath = QFileDialog::getSaveFileName(
            this,
            tr("Export XRechnung as PDF"),
            /// Remove file extension (if any) and append ".pdf" for suggested file name
            xr->url().path().remove(QRegularExpression("\\.[^.]+$"))+".pdf",
            tr("PDF (*.pdf)")
        );
        if (pdfFilePath.isEmpty()) { return; }
        xr->exportPdf(pdfFilePath);
    }
}

void MainWindow::slotSaveAs()
{
    XRWidget *item = qobject_cast<XRWidget*>(ui->_mainToolBox->currentWidget());
    if (item != nullptr) {
        XRechnung *xr = item->xrechnung();
        QString asFilePath = QFileDialog::getSaveFileName(
            this,
            tr("Save XRechnung as"),
            xr->url().path(),
            tr("XML (*.xml)")
        );
        if (asFilePath.isEmpty()) { return; }
        QString oldPath = xr->url().path();
        // Don't save under same name
        if (oldPath == asFilePath) { return; }
        // Make sure the target file doesn't exist (Save as is an overwrite)
        if (QFile::exists(asFilePath)) {
            QFile::remove(asFilePath);
        }
        if (QFile::copy(oldPath, asFilePath)) {
	        statusBar()->showMessage(tr("XRechnung sucessfully saved to given path."));
        } else {
	        QMessageBox::critical(
		        this,
		        tr("Saving XRechnung failed."),
		        tr(
			        "Could not save XRechnung to specified path.\n"
			        "Possible causes are:\n"
			        "  * You don't have the necessary permissions.\n"
			        "  * The target file system is full."
		        )
	        );
        }
    }
}

void MainWindow::slotShowFileList()
{
    int cnt = ui->_mainToolBox->count();

    if (cnt > 0) {
        ui->_mainToolBox->blockSignals(true);
        for (int i=0; i < cnt; i++) ui->_mainToolBox->removeItem(i);
        ui->_mainToolBox->blockSignals(false);
    }
    _xrc.registerFiles(_files);
}

void MainWindow::setFileList(const QList<QUrl>& fileList)
{
    _files = fileList;
}

void MainWindow::slotShowXRechnung(XRechnung *xr)
{
    if (xr == nullptr) return;

    ui->actionSaveAs->setEnabled(false);
    ui->actionExportAsPdf->setEnabled(false);
    ui->_mainToolBox->blockSignals(true);
    int indx = ui->_mainToolBox->addItem(new XRWidget(xr, this), xr->fileName());
    qDebug() << "Added index:" << indx;

    connect( xr, &XRechnung::htmlAvailable, this, [=]() {
        this->slotShowDocumentNo(indx);
    });
    ui->_mainToolBox->setCurrentIndex(indx);
    ui->_mainToolBox->blockSignals(false);
    ui->actionSaveAs->setEnabled(true);
    ui->actionExportAsPdf->setEnabled(true);
}

void MainWindow::slotShowDocumentNo(int newIndx)
{
    XRWidget *item = qobject_cast<XRWidget*>(ui->_mainToolBox->widget(newIndx));

    XRechnung *xr = item->xrechnung();

    const QString h = xr->html();
    ui->_htmlView->setHtml(h);
}

void MainWindow::about()
{
    const QString author = XRView::Info::authors().toHtmlEscaped();
    QMessageBox::about(this, tr("XRView - XRechnung Viewer"),
                             tr("<b>XRechnung Viewer Version %1</b><br/><br/>XRView is a utility to display XRechnung documents.<br/><br/>"
                                "This program is released under GPL v3.<br/>"
                                "Please contribute at <a href=\"https://codeberg.org/openKMU/xrechnung\">https://codeberg.org/openKMU/xrechnung</a><br/><br/>"
                                "Author: %2<br/>"
                                "Copyright %3 Klaas Freitag")
                       .arg(XRView::Version::humanReadable())
                       .arg(author)
                       .arg(XRView::Info::copyrightYear()));
}
