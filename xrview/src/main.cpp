/*
 * XRechnung Viewer Suite
 *
 * Copyright (C) 2023 Klaas Freitag <opensource@freisturz.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"
#include "xrview.h"

#include <QApplication>
#include <QCommandLineParser>
#include <QTranslator>
#include <QUrl>
#include <QObject>
#include <QDebug>
#include <QLibraryInfo>

bool loadTranslation(const QString& filename, const QString& desc)
{
    QLocale locale;

    QTranslator *translator = new QTranslator; // This is a leak, but accepted here.
    QString path;
    QString file{filename};

    const QString localeStr = QString("_%1").arg(locale.bcp47Name());
    file.replace("__", localeStr);

    if (!filename.startsWith(':')) {
        path = QLibraryInfo::path(QLibraryInfo::TranslationsPath);
    }

    bool re{false};
    if (translator->load(file, path)) {
        if (QCoreApplication::installTranslator(translator)) {
            qDebug() << "Successfully loaded" << desc << "from file" << file << "from" << path;
            re = true;
        }
    } else {
        qDebug() << "failed to load translation" << file << "from" << (path.isEmpty() ? QStringLiteral("Empty path") : path) << desc;
    }
    return re;
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName("xrview");
    QCoreApplication::setOrganizationDomain("volle-kraft-voraus.de");
    QCoreApplication::setApplicationName("xrview");
    QCoreApplication::setApplicationVersion(XRView::Version::humanReadable());


    QCommandLineParser parser;
    parser.setApplicationDescription(XRView::Info::description());
    parser.addVersionOption();
    parser.addHelpOption();
    parser.addPositionalArgument(QObject::tr("xrechnung"), QObject::tr("List of xrechnung"), QStringLiteral("[xrechnung...]"));
    parser.process(app);

    loadTranslation(QStringLiteral("qt__.qm"), QStringLiteral("Qt Translation"));
    loadTranslation(QStringLiteral("qtbase__.qm"), QStringLiteral("QtBase Translation"));
    loadTranslation(QStringLiteral(":/i18n/resources/xrview__.qm"), QStringLiteral("App Translations"));

    MainWindow w;
    // get the list of images to load on startup:
    QList<QUrl> filesList;

    for (const QString& file : parser.positionalArguments()) {
        const QUrl argUrl = QUrl::fromLocalFile(file);
        qDebug() << argUrl;
        filesList << argUrl;
    }

    w.show();
    w.setFileList(filesList);

    return app.exec();
}
