/*
 * XRechnung Viewer Suite
 *
 * Copyright (C) 2023 Klaas Freitag <opensource@freisturz.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "xrechnung.h"
#include "qdebug.h"

#include <QFile>
#include <QProcess>
#include <QDateTime>
#include <QSettings>
#include <QTemporaryFile>

QString XRechnung::SaxonJar = QStringLiteral("saxon/jar");
QString XRechnung::SaxonUbl = QStringLiteral("saxon/xslUbl");
QString XRechnung::SaxonHtml = QStringLiteral("saxon/xslHtml");
QString XRechnung::SaxonPdf = QStringLiteral("saxon/xslPdf");
QString XRechnung::SaxonXsl = QStringLiteral("saxon/xsl");

namespace {

QString errorToHtml(const QString& title, const QString& errorStr)
{
    QString htmlStr{"<!DOCTYPE html>"
                    "<html><head><title>%1</title></head>"
                    "<h2>%1</h2>"
                    "<body><pre style=\"background-color: #f5d0d0; font-size: 12px; margin: 5px; padding: 20px;\">%2</pre></body></html>"};

    const auto html = htmlStr.arg(title.toHtmlEscaped(), errorStr.toHtmlEscaped());

    return html;
}

} // End namespace

XRechnung::XRechnung(QObject *parent)
    : QObject{parent}
{
     _tempfile.open();
}

XRechnung::XRechnung(const QUrl& file, QObject *parent)
    : QObject{parent},
      _url(file)
{
    _tempfile.open();
    qDebug() << "Tempfile: " << _tempfile.fileName();
    createUbl();
}

QString XRechnung::html() const
{
    return _html;
}

QUrl XRechnung::url() const
{
    return _url;
}

QString XRechnung::value(const QString& name) const
{
    QDomNodeList elems = _domDoc.elementsByTagName(name);

    if (elems.isEmpty()) {
        qDebug() << "Can not find dom element" << name;
        return QString();
    }

    if (elems.size() > 1)
        qDebug() << "Dom list larger than 1";

    QDomElement elem = elems.at(0).toElement();
    const QString t = elem.text();
    return t;
}

QDomDocument XRechnung::domDocument() const
{
    return _domDoc;
}

void XRechnung::createUbl()
{
    QString file{ _url.toLocalFile() };
    QSettings config;

    const QString saxJar = config.value(SaxonJar).toString();
    const QString xslUBL = config.value(SaxonUbl).toString();

    const QStringList args {
        "-jar", 
        saxJar,
        QString("-s:%1").arg(file),
        QString("-xsl:%1").arg(xslUBL),
        QString("-o:%1").arg(ublFileName()),
     };

    QProcess *process = new QProcess;

    connect(process, &QProcess::finished, this, &XRechnung::slotDomFinished);
    connect(process, &QProcess::errorOccurred, this, &XRechnung::slotErrorOccurred);

    qDebug() << "Starting" << "java" << args;
    process->start("java", args);
}

void XRechnung::slotErrorOccurred(QProcess::ProcessError error)
{
    int exitCode = 1; // general error
    if (error == QProcess::FailedToStart) {
        qDebug() << "failed to start saxon";
    }
}

void XRechnung::slotDomFinished(int /*exitCode*/, QProcess::ExitStatus /*exitStatus*/)
{
    QProcess *p = qobject_cast<QProcess*>(sender());

    const QString errorStr = p->readAllStandardError();
    p->deleteLater();

    if (errorStr.isEmpty()) {
        readDomDocument();

        // start the conversion to html
        createHtml();
    } else {
        // error while creating the ubl
        _html = errorToHtml(tr("Error while creating UBL"), errorStr);
        Q_EMIT htmlAvailable();
        return;
    }
}

void XRechnung::createHtml()
{
    QString file{ _url.toLocalFile() };
    QSettings config;

    const QString saxJar = config.value(SaxonJar).toString();
    const QString xslHtml = config.value(SaxonHtml).toString();

    const QStringList args {
        "-jar",
        saxJar,
        QString("-s:%1").arg(ublFileName()),
        QString("-xsl:%1").arg(xslHtml),
    };

    QProcess *process = new QProcess;

    connect(process, &QProcess::finished, this, &XRechnung::slotHtmlFinished);

    connect(process, &QProcess::errorOccurred, this, &XRechnung::slotErrorOccurred);

    qDebug() << "Starting" << "java" << args;
    process->start("java", args);
}

void XRechnung::slotHtmlFinished(int /*exitCode*/, QProcess::ExitStatus /*exitStatus*/)
{
    QProcess *p = qobject_cast<QProcess*>(sender());

    const QString errorStr = p->readAllStandardError();
    if (!errorStr.isEmpty()) {
        // error while creating the html
        _html = errorToHtml(tr("Error while creating HTML"), errorStr);
    } else {
        _html = p->readAllStandardOutput();
    }
    p->deleteLater();
    Q_EMIT htmlAvailable();
}

void XRechnung::exportPdf(const QString& path)
{
    QString file{ _url.toLocalFile() };
    QSettings config;

    const QString saxJar = config.value(SaxonJar).toString();
    const QString xslPdf = config.value(SaxonPdf).toString();

    const QStringList xsl_args {
        "-jar",
        saxJar,
        QString("-s:%1").arg(ublFileName()),
        QString("-xsl:%1").arg(xslPdf),
    };
    
    const QStringList fop_args {
        "-fo", "-",
        "-pdf", path,
    };

    QProcess *xsl_process = new QProcess;
    QProcess *fop_process = new QProcess;

	xsl_process->setStandardOutputProcess(fop_process);

    connect(xsl_process, &QProcess::errorOccurred, this, &XRechnung::slotErrorOccurred);
    connect(fop_process, &QProcess::errorOccurred, this, &XRechnung::slotErrorOccurred);
    connect(fop_process, &QProcess::finished, this, &XRechnung::slotPdfFopFinished);
    connect(xsl_process, &QProcess::finished, this, &XRechnung::slotPdfXslFinished);

    qDebug() << "Starting" << "java" << xsl_args;
    xsl_process->start("java", xsl_args);
    qDebug() << "Starting" << "fop" << fop_args;
    fop_process->start("fop", fop_args);
}

void XRechnung::slotPdfFopFinished(int /*exitCode*/, QProcess::ExitStatus /*exitStatus*/)
{
    QProcess *p = qobject_cast<QProcess*>(sender());
    qDebug() << "PDF export finished.";
    const QString errorStr = p->readAllStandardError();
    if (!errorStr.isEmpty()) {
	    qDebug() << "Stderr while running fop:\n" << errorStr;
    }
    p->deleteLater();
}

void XRechnung::slotPdfXslFinished(int /*exitCode*/, QProcess::ExitStatus /*exitStatus*/)
{
    QProcess *p = qobject_cast<QProcess*>(sender());
    qDebug() << "PDF XSL tranformation finished.";
    const QString errorStr = p->readAllStandardError();
    if (!errorStr.isEmpty()) {
	    qDebug() << "Stderr while running Saxon:" << errorStr;
    }
    p->deleteLater();
}

bool XRechnung::readDomDocument()
{
    bool re{false};
    QDomDocument doc("InterDoc");
    QFile file(ublFileName());

    if (file.open(QIODevice::ReadOnly)) {

        if (doc.setContent(&file)) {
            qDebug() << "Successfully loaded xml" << file.fileName();
            _domDoc = doc;
            re = true;
        }
    }
    file.close();

    Q_EMIT domDocAvailable();

    return re;
}
